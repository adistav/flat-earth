#!/usr/bin/python3
  
from math import sqrt, pi, sin, cos, acos


def calc_flat(lat1, long1, lat2, long2):
    lat1_angle, lat2_angle = pi/2 - lat1, pi/2 - lat2  # radians from north pole
    return sqrt(lat1_angle**2 + lat2_angle**2 - 2 * lat1_angle * lat2_angle * cos(long2 - long1))


def calc_sphere(lat1, long1, lat2, long2):
    return acos(sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(long2 - long1))


calc_funcs = dict(flat=calc_flat, sphere=calc_sphere)


def unbabylon_angle(exp):
    return sum(float(unit) / 60**i for i, unit in enumerate(exp.split(':'))) * pi / 180


def calc(*args):
    radian_args = list(map(unbabylon_angle, args))
    return {name: func(*radian_args) / pi * 20000
            for name, func in calc_funcs.items()}


def main():
    import sys
    import json

    result = calc(*sys.argv[1:])
    json.dump(result, sys.stdout, indent=4)


if __name__ == '__main__':
    main()
